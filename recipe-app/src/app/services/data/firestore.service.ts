import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import 'firebase/firestore';
import { Recipe } from '../../models/recipe.interface';
import { Observable } from 'rxjs';
import { saved } from 'src/app/models/saved.interface';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  
  constructor(public firestore: AngularFirestore) {}

        createRecipe(
          recipeName:string,
          region: string,
          timeToCook: string,
          ingredients: string,
          instructions:string

        ): Promise<void>{ 
          const id = this.firestore.createId();

          return this.firestore.doc(`recipeList/${id}`).set({
            id,
            recipeName,
            region,
            timeToCook,
            ingredients,
            instructions
          })
        }

        saveRecipe(
          recipe: string,
          user: string,
          name:string,
          time:string
        ): Promise<void>{
          const id = this.firestore.createId();

          return this.firestore.doc(`${user}/${id}`).set({
            id,
            recipe,
            user,
            name,
            time
          })
        }

        updateRecipe(
          id:string,
          recipeName:string,
          region:string,
          timeToCook:string,
          ingredients:string,
          instructions:string
          ){
          return this.firestore.doc(`recipeList/${id}`).update({
                  ingredients: ingredients,
                  recipeName: recipeName,
                  region: region,
                  timeToCook: timeToCook,
                  instructions:instructions
          });
        }

        getRecipeList(): Observable<Recipe[]>{
          return this.firestore.collection<Recipe>('recipeList').valueChanges();
        }
        getRecipeDetail(recipeId: string): Observable<Recipe>{
            return this.firestore.collection('recipeList').doc<Recipe>(recipeId).valueChanges();
        }
        deleteRecipe(recipeId: string): Promise<void>{
          
          return this.firestore.doc(`recipeList/${recipeId}`).delete();
        }
        getSavedRecipe(user): Observable<saved[]>{
          return this.firestore.collection<saved>(`${user}`).valueChanges();
        }
        


}

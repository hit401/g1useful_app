import { Component } from '@angular/core';
import { Observable } from 'rxjs';
//used to display recipes
import { OnInit } from '@angular/core';
import { FirestoreService } from '../services/data/firestore.service';
import { saved } from '../models/saved.interface';
import { Recipe } from '../models/recipe.interface';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { LoadingController, AlertController } from '@ionic/angular';
import {Router} from '@angular/router'

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{
      public savedRecipe:Observable<saved[]>;

  ngOnInit(){
    var user = localStorage.getItem("email");
    this.savedRecipe = this.firestoreService.getSavedRecipe(user);
    

  };

  constructor(
    private firestoreService: FirestoreService,
    public loadingCtrl: LoadingController,
    private router: Router
  ) {}

  

}

import { Component } from '@angular/core';

//used to display recipes
import { OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FirestoreService } from '../services/data/firestore.service';
import { Recipe, } from '../models/recipe.interface';
import { Tab3Page} from "../tab3/tab3.page";
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { LoadingController, AlertController } from '@ionic/angular';
import {Router} from '@angular/router'





@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  public recipeList: Observable<Recipe[]>;
  user = localStorage.getItem("email");
  
  ngOnInit() {
    this.recipeList = this.firestoreService.getRecipeList();
  }
  constructor(
    private firestoreService: FirestoreService,
    public loadingCtrl: LoadingController,
    private router: Router
  ) { 
    
  }

  async save(recipeId: string,recipeName: string,recipeTime: string){
        const loading = await this.loadingCtrl.create();
        const recipe = recipeId;
        const user = localStorage.getItem("email");
        const name = recipeName
        const time = recipeTime
        console.log(user);
        this.firestoreService
        .saveRecipe(recipe,user,name,time).then(
          () => {
                  this.router.navigateByUrl('');;
        },
        error => {
          loading.dismiss().then(() => {
            console.error(error);
          });
        }
    );


        
        

  }
  
}

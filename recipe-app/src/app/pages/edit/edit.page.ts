import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Recipe } from '../../models/recipe.interface';
import { FirestoreService } from '../../services/data/firestore.service';
import { LoadingController, AlertController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators,ReactiveFormsModule  } from '@angular/forms';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  public recipe: Recipe;
  public editRecipe:FormGroup;
  constructor(
    private firestoreService: FirestoreService,
    private route: ActivatedRoute,
    public loadingCtrl: LoadingController,
    private alertController: AlertController,
    private router: Router,
    private formBuilder: FormBuilder
  ) { 
    this.editRecipe = formBuilder.group({
      recipeName:['',Validators.required],
      region:['',Validators.required],
      timeToCook:['',Validators.required],
      ingredients:['',Validators.required],
      instructions:['',Validators.required]
    });
  }

  ngOnInit() {
    const recipeId: string = this.route.snapshot.paramMap.get('id');
    this.firestoreService.getRecipeDetail(recipeId).subscribe(recipe =>{
      this.recipe = recipe;
    })
  }

  async updateRecipe(){
    const loading = await this.loadingCtrl.create();
    const id = this.recipe.id

    const recipeName =  this.editRecipe.value.recipeName;
    const region =      this.editRecipe.value.region;
    const timeToCook =  this.editRecipe.value.timeToCook;
    const ingredients = this.editRecipe.value.ingredients;
    const instructions = this.editRecipe.value.instructions;

    this.firestoreService.updateRecipe(id,recipeName,region,timeToCook,ingredients,instructions);



  }



}

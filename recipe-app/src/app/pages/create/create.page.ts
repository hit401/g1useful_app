import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators,ReactiveFormsModule  } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { FirestoreService } from '../../services/data/firestore.service';
import {Router} from '@angular/router'


@Component({
  selector: 'app-create',
  templateUrl: './create.page.html',
  styleUrls: ['./create.page.scss'],
})
export class CreatePage implements OnInit {

  public createRecipeForm:FormGroup;

  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private firestoreService: FirestoreService,
    formBuilder: FormBuilder,
    private router: Router
  ) { 
    this.createRecipeForm = formBuilder.group({
      recipeName:['',Validators.required],
      region:['',Validators.required],
      timeToCook:['',Validators.required],
      ingredients:['',Validators.required],
      instructions:['',Validators.required]
    });
  }

  async createRecipe(){
    const loading = await this.loadingCtrl.create();
    
    const recipeName =  this.createRecipeForm.value.recipeName;
    const region =     this.createRecipeForm.value.region;
    const timeToCook =  this.createRecipeForm.value.timeToCook;
    const ingredients = this.createRecipeForm.value.ingredients;
    const instructions = this.createRecipeForm.value.instructions;


    this.firestoreService
    .createRecipe(recipeName,region,timeToCook,ingredients,instructions).then(
      () => {
          this.router.navigateByUrl('');;
        },
        error => {
          loading.dismiss().then(() => {
            console.error(error);
          });
        }
    );
    //return await loading.present();
  }

  ngOnInit() {
  }

}
